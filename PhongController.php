<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Collection;
use DB;


use App\Models\Phong;
use App\Models\LoaiPhong;
use App\Models\Tang;

class PhongController extends Controller
{
    public function xem_phong(Request $rq)
	{
		$title = 'Xem tất cả phòng';
		$array_tang = Tang::get();
		$array_loai_phong = LoaiPhong::get();
		$ma_tang = $rq->ma_tang;
		$ma_loai_phong = $rq->ma_loai_phong;
		// $loai_phong = LoaiPhong::where('ma',$ma_loai_phong)->first();
		// $ten_loai_phong = $loai_phong->ten;
		// $ten_tang = Tang::find($ma_tang)->ten;

		// sinh viên kèm điểm
		$query = Phong::query();
		if(!empty($ma_tang)){
			$query->where('ma_tang',$ma_tang);
		}
		if(!empty($ma_loai_phong)){
			$query->where('ma_loai_phong',$ma_loai_phong);
		}
		$array_phong = $query->get();
		// $array_phong = Phong::where('ma_loai_phong',$ma_loai_phong)
		// ->leftJoin('phong','phong.ma_loai_phong','loai_phong.ma')
		// ->where('ma_loai_phong',$ma_loai_phong)
		// ->get();

		// $array = [];
		// foreach ($array_loai_phong as $each) {
		// 	$ma_loai_phong = $each->ma;
		// 	$loai_phong         = $each->loai_phong;

		// 	$array[$ma_loai_phong] = $loai_phong;
		// }

		return view('phong.xem_phong',[
			'array_tang'       => $array_tang,
			'array_loai_phong'       => $array_loai_phong,
			'array_phong' => $array_phong,
			'ma_tang'          => $ma_tang,
			'ma_loai_phong'          => $ma_loai_phong,
			// 'ten_tang'         => $ten_tang,
			// 'array'           => $array,
            'title' => $title,

		]);
	}
	public function cap_nhat_diem(Request $rq)
	{
		$array_diem = $rq->array_diem;
		$ma_loai_phong     = $rq->ma_loai_phong;
		foreach ($array_diem as $ma_sinh_vien => $diem) {
			if(isset($diem)){
				Diem::updateOrCreate([
					'ma_loai_phong'       => $ma_loai_phong,
					'ma_sinh_vien' => $ma_sinh_vien,
				],[
					'diem' => $diem
				]);
			}
			else{
				Diem::where([
					'ma_loai_phong'       => $ma_loai_phong,
					'ma_sinh_vien' => $ma_sinh_vien,
				])->delete();
			}
		}
	}
}

