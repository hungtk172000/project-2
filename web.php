<?php

Route::get('','LoginController@view_login')->name('view_login');
Route::post('process_login','LoginController@process_login')->name('process_login');

Route::group(['middleware' => 'CheckAdmin'],function(){
	Route::get('welcome','Controller@welcome')->name('welcome');
	Route::get('logout','LoginController@logout')->name('logout');

	$controller = "KhachHangController";
	Route::group(["prefix" => "khach_hang","as" => "khach_hang."],function() use ($controller){
		Route::get("","$controller@view_all")->name("view_all");
		Route::get("view_insert","$controller@view_insert")->name("view_insert");
		Route::post("process_insert","$controller@process_insert")->name("process_insert");
		Route::get("view_update/{ma}","$controller@view_update")->name("view_update");
		Route::post("process_update/{ma}","$controller@process_update")->name("process_update");
		Route::get("delete/{ma}","$controller@delete")->name("delete");
		
		// Route::get("view_array_sinh_vien_by_lop/{ma}","$controller@view_array_sinh_vien_by_lop")->name("view_array_sinh_vien_by_lop");
	});

	$controller = "DichVuController";
	Route::group(["prefix" => "dich_vu","as" => "dich_vu."],function() use ($controller){
		Route::get("","$controller@view_all")->name("view_all");
		Route::get("view_insert","$controller@view_insert")->name("view_insert");
		Route::post("process_insert","$controller@process_insert")->name("process_insert");
		Route::get("view_update/{ma}","$controller@view_update")->name("view_update");
		Route::post("process_update/{ma}","$controller@process_update")->name("process_update");
		Route::get("delete/{ma}","$controller@delete")->name("delete");
	});
	$controller = "PhongController";
	Route::group(["prefix" => "phong","as" => "phong."],function() use ($controller){
		Route::get("","$controller@xem_phong")->name("xem_phong");
		Route::post("cap_nhat_phong","$controller@cap_nhat_phong")->name("cap_nhat_phong");

		// Route::get("view_array_phong_by_loai_phong/{ma}","$controller@view_array_phong_by_loai_phong")->name("view_array_phong_by_loai_phong");
	});
});




Route::get('test','Controller@test');
Route::get('show','Controller@show');
Route::post('post_test','Controller@post_test')->name('post_test');


