@extends('welcome')

@section('content')
<form action="{{ route('phong.xem_phong') }}">
	{{ csrf_field() }}
	Chọn tầng
	<select name="ma_tang">		
		<option value="0">--Chọn Tầng--</option>
		@foreach ($array_tang as $tang)
			<option value="{{ $tang->ma }}"
			@if ($ma_tang == $tang->ma)
				selected 
			@endif
			>
				{{ $tang->ten }}
			</option>
		@endforeach
	</select>
	Chọn loại phòng
	<select name="ma_loai_phong">
		<option value="0">--Chọn Loại Phòng--</option>
		@foreach ($array_loai_phong as $loai_phong)
			<option value="{{ $loai_phong->ma }}"
			@if ($ma_loai_phong == $loai_phong->ma)
				selected 
			@endif
			>
				{{ $loai_phong->loai_phong }}
			</option>
		@endforeach
	</select>
	<button>Xem Tất Cả Phòng</button>
</form>


<table class="table table-hover">
	
	<tr>
		<th>Số Phòng</th>
		<th>Loại Phòng</th>
		<th>Mô Tả</th>
		<th>Xem chi tiết</th>
	</tr>
	@foreach ($array_phong as $phong)
		<tr>
			<td>
				{{ $phong->so_phong }}
			</td>
			<td>
				{{ $phong->loai_phong->loai_phong }}
			</td>
			<td>
				{{ $phong->loai_phong->mo_ta }}
			</td>
			<td>
				<a href="">Xem chi tiết</a>
			</td>
		</tr>
	@endforeach
</table>

@endsection
